<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\RoleRequest;
use App\Role;

class RolesController extends Controller
{
    public function index()
    {
    	return view('roles.add_roles');
    }
    public function addRole(RoleRequest $request)
    {
    	try {
    		$save_role = Role::updateOrCreate(['name'=>$request->role,'guard_name'=>$request->role]);
    		if ($save_role) {
    			toastr()->success('Role Created Successfully');
    			//return $this->index();
    		}
    		else{
    			toastr()->error('Role not Created');
    			//return response()->json(['status'=>false]);
    		}
    		
    	} catch (Exception $e) {
    		toastr()->warning($e);
    		//return response()->json(['status'=>$e]);
    	}
    }
}
